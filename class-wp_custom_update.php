<?php
/**
 * Class for a custom update routine in WordPress
 *
 * PHP version 5.3
 *
 * @category   PHP
 * @package    WordPress
 * @subpackage class-wp_custom_update
 * @author     Ralf Albert <me@neun12.de>
 * @license    GPLv3 http://www.gnu.de/documents/gpl.de.html
 * @version    0.3.2
 * @link       http://wordpress.com
 */

function start_update_checker( $file ){
	return new WP_Custom_Update( $file );
}

if( ! class_exists( 'WP_Custom_Update' ) ){
	class WP_Custom_Update
	{
		/**
		 * 
		 * Slug for update-cron
		 * @var string
		 * @access constant
		 * @see http://codex.wordpress.org/Function_Reference/wp_schedule_event
		 */
		const UPDATE_ROUTINE = 'wp_custom_updater_update_routine';
		
		/**
		 * 
		 * Interval for update checks
		 * @var string
		 * @access constant
		 * @see http://codex.wordpress.org/Function_Reference/wp_schedule_event
		 */
		const UPDATE_INTERVAL = 'twicedaily'; // hourly|twicedaily|daily
		
		/**
		 * 
		 * Name for the option-table
		 * @var string
		 * @access constant
		 */
		const TRANSIENT_NAME = 'wp_custom_update';
		
		/**
		 * 
		 * Time in second the site-transient is valid
		 * @var int
		 * @access constant
		 */
		const TRANSIENT_TIME = 86400;
		
		/**
		 * 
		 * Path to theme/plugin
		 * @var string
		 * @access protected
		 */
		protected $file = '';
		
		/**
		 * 
		 * Hash to store theme/plugin-data in database
		 * @var string
		 * @access protected
		 */
		protected $file_hash = '';
		
		/**
		 * 
		 * Array with settings of the theme/plugin 
		 * @var array
		 * @access protected
		 */
		protected $file_data;
		
		/**
		 * 
		 * Flag for cached fiel-data
		 * @var bool
		 * @access protected
		 */
		protected $is_cached;
		
		/**
		 * 
		 * Cache for http-response
		 * @var array
		 * @access protected
		 */
		protected $response = array();
	
		/**
		 * 
		 * WordPress Error-Object
		 * @var object
		 * @access protected
		 */
		protected $wp_error = NULL;
		
		/**
		 * 
		 * Constructor
		 * Expect a valid fielname. Create an array with needed filedata and initiliazes the cron-job
		 * @param string $file
		 */
		public function __construct( $file ){
			
			// hook up error-messages at first. so we can output every error
			add_action( 'admin_notices', array( &$this, 'show_errors' ) );
	
			// check filename
			if( '' == $file || ! is_string( $file ) ){
				
				$this->add_error( "Missing filename in constructor", __METHOD__ );
				return FALSE;
				
			} elseif( ! is_readable( $file ) ){
				
				$this->add_error( "Can not read from <em>{$file}</em>", __METHOD__ );
				
			} else {
				
				$this->file		 = $file;
				$this->file_hash = md5( $file );
				$this->file_data = array();
				
			}
			
			// setup the data
			$this->_setup_file_data();

			// exclude plugin/theme from updates and initialize the cron-job
			$this->_init();
				
		}
		
		/**
		 * 
		 * Check if errors occur
		 * @param none
		 * @return bool
		 * @access public
		 */
		public function have_errors(){
			
			return is_wp_error( $this->wp_error );
			
		}
		
		/**
		 * 
		 * Returns an array with error-messages or false if no error occurs
		 * @param none
		 * @return array|FALSE
		 * @access public
		 */
		public function get_errors(){
			
			if( is_wp_error( $this->wp_error ) ){
				
				if( isset( $this->wp_error->errors['wp_custom_updater'] ) )
					return $this->wp_error->errors['wp_custom_updater'];
				else
					return NULL;
				
			} else {
				
				return NULL;
				
			}
			
		}
		
		/**
		 * 
		 * Display errors on admin-screen as notice
		 * @param none
		 * @return none
		 * @access public
		 */
		public function show_errors(){
			
				if( $this->have_errors() ){
					
					echo '<div class="error">';
					echo '<h3>WP Custom Updater Errors</h3>';
					echo '<p>WP Custom Updater was not correctly initialized. For more details see the list below.</p>';
					echo '<ol>';
					foreach( $this->get_errors() as $error )
						echo "<li>{$error}</li>";
					echo '</ol>';
					echo '</div>';
	
				}
				
		}
		
		/**
		 * 
		 * Add an error to the internal error-list. Add the class and method if WordPress runs in debug-mode.
		 * @param string $message
		 * @param string $method [optional]
		 * @access protected
		 */
		protected function add_error( $message = '', $method = '' ){
			
			if( '' != $message ){
				
				// add additional info if debug-mode is turned on 
				if( ! empty( $method ) && ( defined( 'WP_DEBUG' ) && TRUE === WP_DEBUG ) )
					$message = sprintf( '%s: %s', $method, $message );
				
				if( NULL == $this->wp_error )
					$this->wp_error = new WP_Error( 'wp_custom_updater', $message );
				else
					$this->wp_error->add( 'wp_custom_updater', $message );
			
			}
			
		}
		
		/**
		 * 
		 * Remove the plugin/theme from automatic updates and schedule the cron-job
		 * @param none
		 * @return none
		 * @access protected
		 */
		protected function _init(){
			
			// prevent auto-updates and initialize own update routine
			if( FALSE == $this->have_errors() ){
				
				add_filter( 'http_request_args', array( &$this, 'no_updates' ), 5, 2 );
				
				if( FALSE == wp_next_scheduled( self::UPDATE_ROUTINE ) )
					wp_schedule_event( time(), self::UPDATE_INTERVAL, self::UPDATE_ROUTINE );
					
				add_action( self::UPDATE_ROUTINE, array( &$this, 'check_for_updates' ) );
				
			}
			
		}
		
		/**
		 * 
		 * Method to remove the plugin/theme from automatic updates
		 * @param array $r
		 * @param string $url
		 * @return none
		 * @access public
		 * @internal hooked by filter 'http_request_args' in ::_init()
		 */
		public function no_updates( $r, $url ){
	
			// bail if the url points to wordpress.org
			if( 1 == preg_match( '#http://api.wordpress.org/(plugins|themes)/update-check#', $url ) )
				return $r;
	
			// check if the slug was set
			$slug = isset( $this->file_data['slug'] ) ? $this->file_data['slug'] : '';
	
			if( empty( $slug ) ){
				
				$this->add_error( "Missing or empty slug in settings.", __METHOD__ );
				return $r;
				
			}
				
			// check if the given type is valid
			$type = isset( $this->file_data['type'] ) ? $this->file_data['type'] : '';
			
			if( empty( $type ) || ! in_array( $type, array( 'plugins', 'themes' ) ) )
				$this->add_error( "Unknown type {$type}.", __METHOD__ );
			
			// exclude plugin/theme from list only if it is registered 
			if( isset( $r['body'][$type] ) ){
				
				$data = unserialize( $r['body'][$type] );
				
				// check if plugin/theme was registered
				if( ! isset( $data[$slug] ) ){
					
					$this->add_error( "<em>{$slug}</em> was not found in {$type}-list. Could not exclude plugin/theme from updating.", __METHOD__ );
					
				} else {
					
					// exclude plugin/theme from update-list
					unset( $data[$slug] );
					$r['body'][$type] = serialize( $data );
					
				}
				
			}
			
			return $r;
			
		}
	
		/**
		 * 
		 * Checks if there is an update for the theme/plugin
		 * @param none
		 * @return bool
		 * @access public
		 * @internal hooked by cron-job in ::_init()
		 */
		public function check_for_updates(){
			
			if ( defined( 'WP_INSTALLING' ) )
				return FALSE;  

			// check if the initialization was ok
			if( empty( $this->file_data ) )
				return FALSE;

			// check if the cached data are up to date
			if( TRUE === $this->is_cached ){

				$file_data = $this->_autodetect();
				
				if( $this->file_data != $file_data ){

					// update the cache
					$this->file_data = $file_data;
					$this->cache_file_data( $file_data );
					unset( $file_data );
					
				}
				
			}
			
				
			$current_version = &$this->file_data['current_version'];
			$slug			 = &$this->file_data['slug'];
			$type			 = &$this->file_data['type'];
			$url			 = &$this->file_data['url'];
				  
			list( $version, $package_url ) = explode( '|', $this->get_response_body( $this->file_data['update_url'] ) );
	
			if( version_compare( $version, $current_version, '<' ) )
				return false;
		
			$args = array(
				'slug'			=> $slug,
				'new_version'	=> $version,
				'url'			=> $url,
				'package'		=> $package_url,
			);
		
			if( 'plugins' == $type )
				$args = (object) $args;
	
			$_transient = get_site_transient( 'update_' . $type );
			$_transient->response[$slug] = $args; 
			
			set_site_transient( 'update_' . $type, $_transient, self::TRANSIENT_TIME );
	
		}
			
		/**
		 * 
		 * Setup an array with data for the update routine.
		 * Validate and sanitize the data.
		 * @param array
		 * @access protected
		 */
		protected function _setup_file_data(){
			
			// get file-data from database
			$cached_data = get_site_transient( self::TRANSIENT_NAME );
			
			// use stored file-data or autodetect the file-data
			if( ! empty( $cached_data ) && isset( $cached_data[$this->file_hash] ) ){
				
				// get file-data form database
				$this->file_data = unserialize( $cached_data[$this->file_hash] );
				$this->is_cached = TRUE;

				return TRUE;
				
			} else {
				
				// try to autodetect the settings
				$cached_data 	 = array();
				$this->file_data = $this->_autodetect();
				$this->is_cached = FALSE;

				// stop on errors while collecting file-data
				if( TRUE == $this->have_errors() )
					return FALSE;

			}
			
			
			/*
			 * type				=> plugins | themes
			 * slug				=> basename( dirname( __FILE__ ) ) -> themes
			 * 					   basename( dirname( __FILE__ ) ) / basename( __FILE__ ) -> plugins
			 * url				=> plugin/theme-url. points to the homepage of the plugin/theme
			 * update_url		=> url (points to the url with update informations in format "version|path_to_package")
			 * package_url		=> url pointing to the package
			 * current_version	=> current version of the plugin/theme
			 */
			$defaults = array(
				'type'				=> 'plugin',
				'slug'				=> '',
				'url'				=> '',
				'update_url'		=> '',
				'package_url'		=> '',
				'current_version'	=> '0',
			);
			
			// merge defaults with collected file-data
			$this->file_data = array_merge( $defaults, $this->file_data );
			
			// validate settings. check if an value is empty
			$can_be_empty = array( 'url' );
			
			foreach( array_keys( $defaults ) as $key ){
				
				if( in_array( $key, $can_be_empty ) )
					continue;
				elseif( empty( $this->file_data[$key] ) )
					$this->add_error( "{$key} could not be empty.", __METHOD__ );
			}
			
			// caching the results
			$this->cache_file_data( $this->file_data );
											
			return $this->have_errors();
			
		}
			
		/**
		 * 
		 * Store the file-data in options table for caching
		 * @param array $filedata
		 * @return void
		 * @access protected
		 */
		protected function cache_file_data(){
			
			if( FALSE == $this->have_errors() ){
				
				$cached_data = get_site_transient( self::TRANSIENT_NAME );

				$cached_data[$this->file_hash] = serialize( $this->file_data );
				
				//$result = update_option( self::OPTION_NAME, $cached_data );
				$result = set_site_transient( self::TRANSIENT_NAME, $cached_data, self::TRANSIENT_TIME );

				if( FALSE == $result )
					$this->add_error( "Could not cache file-data", __METHOD__ );
				
			}
			
		}
		
		/**
		 * 
		 * Autodetect the file-data
		 * @param string $file
		 * @return bool
		 * @access protected
		 * @internal calls ::_setup_file_data()
		 */
		protected function _autodetect(){
			
			// prepare vars to avoid strict errors
			$_DS = $is_what = $slug = $file = $pattern = $update_string = '';
			$types = $filepath = $args = $headers = $filedata = array();
			
			/*
			 * creating a escaped directory seperator
			 * \\ for windows systems and / for *nix systems
			 */
			$_DS = DIRECTORY_SEPARATOR;
			$_DS =  chr(92) == $_DS ? $_DS = '\\\\' : $_DS = '/';
	
			/*
			 * try to find the type (plugin or theme) and extract the path to plugin/theme
			 */
			preg_match( sprintf( '#%1$s(plugins|themes)%1$s#', $_DS ), $this->file, $types );
			
			if( ! empty( $types ) ){
				$is_what = isset( $types[1] ) ? $types[1] : '';
				
				// get the plugin/theme-slug
				if( ! empty( $is_what ) ){
					
					// extract folder and filename
						
						switch( $is_what ){
							case 'plugins':
								$slug = sprintf( '%s/%s', basename( dirname( $this->file ) ), basename( $this->file ) );
							break;
							
							case 'themes':
								$slug = basename( dirname( $this->file ) );
							break;
							
							default:
								$slug = '';
							break;
						}
						
					// replace backslashes with normal slashes. windows, u know!?
					if( ! empty( $slug ) )
						$slug = str_replace( chr(92), '/', $slug );	
				} 			
			}
			
			$args['type']	= &$is_what;
			$args['slug']	= &$slug;
	
			/*
			 *  get plugin/theme details
			 */
			if ( ! function_exists( 'get_file_data' ) )
				require_once( ABSPATH . '/includes/functions.php' );
			
			// extract update-url and version from file-header
			$headers = array(
				'UpdateURL'	=> 'Update URL',
				'Version'	=> 'Version',
				'PluginURI'	=> 'Plugin URI',
				'URI'		=> 'Theme URI',
			 );
			
			// if it is a theme, get data from style.css
			if( 'themes' == $is_what ){
				
				$pattern = sprintf( '#(?<=\\\\)%s#', basename( $this->file ) );
				// do not modify the original file-path
				$file = preg_replace( $pattern, 'style.css', $this->file );
							
			} else {
				$file = $this->file;
			}
			 
			$filedata = get_file_data( $file, $headers );
	
			// get the location where to find update informations
			if( ! isset( $filedata['UpdateURL'] ) || empty( $filedata['UpdateURL'] ) ){
				$this->add_error( "No update-url found in file-header.<br>Please set 'UpdateURL: [url]' in file-header.", __METHOD__ );
			} else {
				// check if the update-url is valid
				if( $this->validate_url( $filedata['UpdateURL'] ) )
					$args['update_url'] = $filedata['UpdateURL'];
			}
			
			// get the current version of the plugin/theme
			if( ! isset( $filedata['Version'] ) || empty( $filedata['Version'] ) ){
				$this->add_error( "No version found in file-header.", __METHOD__ );
			} else {
				$args['current_version'] = $filedata['Version'];
			}
			
			// get the url to the plugin/theme homepage
			if( isset( $filedata['PluginURI'] ) && ! empty( $filedata['PluginURI'] ) ){
				
				$args['url'] = $filedata['PluginURI'];
				
			} elseif( isset( $filedata['URI'] ) && ! empty( $filedata['URI'] ) ){
				
				$args['url'] = $filedata['URI'];
				
			} else {
				
				$args['url'] = FALSE;
				
			}
			
			// stop on error
			if( TRUE === $this->have_errors() )
				return FALSE;
					
			/*
			 * validate data from update-string
			 */
			$update_string = isset( $args['update_url'] ) ? $this->get_response_body( $args['update_url'] ) : '';
			
			if( ! empty( $update_string ) ){		
				$args['package_url'] = $this->get_package_url( $update_string );
			}
		
			/*
			 * test if the package-url is valid
			 */
			if( ! empty( $args['package_url'] ) ){
				
				// test the package-url
				if( FALSE == $this->validate_url( $args['package_url'] ) )
					return FALSE;
				
			}
						
			/*
			 * finish extraction and validation.
			 */
			return $args;
	
		}
		
		/**
		 * 
		 * Validate if the given url and is readable
		 * @param string $url
		 * @return bool
		 * @access protected
		 */
		protected function validate_url( $url ){
			
			if( empty( $url ) || ! is_string( $url ) )
				return FALSE;
				
			// check if the url have a valid format
			if( FALSE === filter_var( $url, FILTER_VALIDATE_URL ) ){
				$this->add_error( "<em>{$url}</em> is not a valid url.", __METHOD__ );
				return FALSE;
			}			
			
			$code = (int) $this->get_response_code( $url );
			
			switch( $code ){
				case 404:
					$this->add_error( "File <em>{$url}</em> not found", __METHOD__ );
					return  FALSE;
				break;
				
				case 200:
					return  TRUE;
				break;
				
				default:
					$this->add_error( "Error while opening <em>{$url}</em>. HTTP-Code: <em>{$code}</em>", __METHOD__ );
					return FALSE;
				break;
			}
			
		}
		
		/**
		 * 
		 * Extracts the package-url from the update-string
		 * @param string $update_string
		 * @return string|bool
		 * @access protected
		 */
		protected function get_package_url( $update_string ){
	
			if( empty( $update_string ) || ! is_string( $update_string ) )
				return FALSE;   
	
			$package_url = FALSE;
			$data		 = explode( '|', $update_string );
	
			if( empty( $data ) || 2 > count( $data ) )			
				$this->add_error( "Error in update string. Expecting format [version]|[update-url].<br>Found <em>{$update_string}</em>", __METHOD__ );
			else
				$package_url = $data[1];
	
			return $package_url;
	                
		}
		
		/**
		 * 
		 * Get the body of a wp_remote_get response
		 * @param string $url
		 * @return string
		 * @access protected
		 */
		protected function get_response_body( $url ){
			
			if( empty( $url ) || ! is_string( $url ) )
				return FALSE;
			
			$data = $this->get_response( $url );
	
			return isset( $data['body'] ) ? $data['body'] : FALSE;
			
		}
		
		/**
		 * 
		 * Get the http-status code of a wp_remote_get response
		 * @param string $url
		 * @return integer
		 * @access protected
		 */
		protected function get_response_code( $url ){
			
			if( empty( $url ) || ! is_string( $url ) )
				return FALSE;
			
			$data = $this->get_response( $url );
			
			return isset( $data['response']['code'] ) ? $data['response']['code'] : 0;
			
		}
		
		/**
		 * 
		 * Prepare a wp_remote_get call and returns the response
		 * @param string $url
		 * @return array
		 * @access protected
		 */
		protected function get_response( $url ){
			
			if( empty( $url ) || ! is_string( $url ) )
				return FALSE;
				
			// create a unique hash for caching
			$url_hash = md5( $url );
			
			// query cache
			if( isset( $this->response[$url_hash] ) && ! empty( $this->response[$url_hash] ) )
				return $this->response[$url_hash];
			
			// cache miss, do a http-request
			global $wp_version;
			
			$options = array(  
				'timeout'		=> ( ( defined('DOING_CRON') && DOING_CRON ) ? 30 : 3),  
				'user-agent'	=> 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' ),
				'sslverify'		=> FALSE  
			);  
			  
			$response = wp_remote_get( $url, $options );
			
			if( is_wp_error( $response ) ){
				
				$this->add_error( "Error while opening <em>{$url}</em>.<br>WP-Error: <em>{$response->get_error_message()}</em>", __METHOD__ );
				return FALSE;
				
			} else {
				
				$this->response[$url_hash] = $response;
				return $response;
				
			}	
			
		}
		
	} // .end class WP_Cusom_Update
	
} // .end if-class-exists