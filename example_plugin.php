<?php
/**
 * Plugin Name: WP Custom Update Example Plugin
 * Plugin URI: 
 * Description: Host your own plugin updates
 * Version: 0.1
 * Author: Ralf Albert
 * Author URI: http://yoda.neun12.de
 * Text Domain:
 * Domain Path:
 * Network:
 * Update URL: http://localhost/testdownload/wp_custom_update.info
 */


! defined( 'ABSPATH' ) and die( "Cheetin' uh!?" );

if( ! class_exists( 'CustomUpadteTest' ) ){
	
	add_action( 'admin_init', function(){ new CustomUpdateTest; } );
	
	class CustomUpdateTest
	{
		public function __construct(){
						
			// the recommended way to include and start the updater
			if( ! function_exists( 'start_update_checker' ) ){
				
				include_once 'class-wp_custom_update.php';
				
			} else {
				
				$updater = start_update_checker( __FILE__ );
	
			}

								
			// the debugging-part
			if( defined( 'WP_DEBUG' ) && TRUE === WP_DEBUG ){
				if( $updater instanceof WP_Custom_Update ){				
					// check immediately for new updates
					// this could be useful, e.g. give the user the possibility checking plugins and themes on demand
					$updater->check_for_updates();
				}
			}
			
		}		
	}
		
}